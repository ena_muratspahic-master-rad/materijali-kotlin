package com.ena.muratspahic.materijalikotlin.adapters.album

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.ena.muratspahic.materijalikotlin.R
import com.ena.muratspahic.materijalikotlin.model.Album
import kotlinx.android.synthetic.main.list_element_child.view.*

class AlbumViewHolder(
        inflater: LayoutInflater,
        parent: ViewGroup,
        albumClickCallback: (album: Album) -> Unit
) : RecyclerView.ViewHolder(inflater.inflate(R.layout.list_element_child, parent, false)) {

    lateinit var album: Album

    init {
        itemView.setOnClickListener { albumClickCallback(album) }
    }


    fun bind(album: Album, position: Int) {
        this.album = album

        album.apply {
            itemView.numberTextView.text = String.format((position + 1).toString(), "%s.")
            itemView.titleTextView.text = this.name
        }
    }

}
