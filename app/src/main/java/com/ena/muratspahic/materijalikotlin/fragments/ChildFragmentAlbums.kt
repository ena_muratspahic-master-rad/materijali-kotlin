package com.ena.muratspahic.materijalikotlin.fragments

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import com.ena.muratspahic.materijalikotlin.R
import com.ena.muratspahic.materijalikotlin.adapters.album.AlbumAdapter
import com.ena.muratspahic.materijalikotlin.database.MusicianDBHelper
import com.ena.muratspahic.materijalikotlin.model.Album
import com.ena.muratspahic.materijalikotlin.utils.Constants
import kotlinx.android.synthetic.main.child_fragment.*

class ChildFragmentAlbums : Fragment() {

    private lateinit var mAlbums : ArrayList<Album>

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.child_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        childTextView.text = getString(R.string.similar_musicians)

        if (arguments != null && arguments!!.containsKey(Constants.MUSICIAN_SPOTIFY_ID)) {
            val musicianId = arguments!!.getString(Constants.MUSICIAN_SPOTIFY_ID)

            if (musicianId != null && context != null) {
                // Get from database and set album adapter
                val db = MusicianDBHelper(context!!)
                mAlbums = db.getAllAlbumsFromMusician(musicianId)
                setAlbumsAdapter()
            }
        }
    }

    private fun setAlbumsAdapter() {
        val albumAdapter = AlbumAdapter(::albumClicked)
        childListView.apply {
            layoutManager = LinearLayoutManager(context)
            itemAnimator = DefaultItemAnimator()
            adapter = albumAdapter
        }
        albumAdapter.mData = mAlbums
    }

    private fun albumClicked(album: Album) {
        val openOfficialPage = Intent(Intent.ACTION_VIEW)
        openOfficialPage.data = Uri.parse(album.spotifyLink)
        startActivity(openOfficialPage)
    }

}