package com.ena.muratspahic.materijalikotlin.model

class Search {

    var id: Int? = null
    var searchText: String = ""
    var timeStamp: String? = null
    var status: Boolean = false

    constructor()

    constructor(searchText: String, timeStamp: String, status: Boolean) : this() {
        this.searchText = searchText
        this.timeStamp = timeStamp
        this.status = status
    }
}