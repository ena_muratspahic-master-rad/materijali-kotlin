package com.ena.muratspahic.materijalikotlin.utils

class DatabaseConstants {

    companion object {
        const val DATABASE_NAME = "databaseKotlin.db"
        const val DATABASE_VERSION = 1

        // MUSICIANS
        const val DATABASE_TABLE_MUSICIANS = "musicians"
        const val MUSICIAN_ID = "id"
        const val MUSICIAN_SPOTIFY_ID = "spotify_id"
        const val MUSICIAN_NAME = "name"
        const val MUSICIAN_GENRE = "genre"
        const val MUSICIAN_OFFICIAL_WEB = "official_web"
        const val MUSICIAN_IMAGE_URL = "image_url"

        // SONGS
        const val DATABASE_TABLE_SONGS = "songs"
        const val SONG_ID = "id"
        const val SONG_SPOTIFY_ID = "spotify_id"
        const val SONG_NAME = "name"
        const val SONG_MUSICIAN_ID = "musician_id"

        // ALBUMS
        const val DATABASE_TABLE_ALBUMS = "albums"
        const val ALBUM_ID = "id"
        const val ALBUM_SPOTIFY_ID = "spotify_id"
        const val ALBUM_NAME = "name"
        const val ALBUM_SPOTIFY_LINK = "spotify_link"
        const val ALBUM_MUSICIAN_ID = "musician_id"

        // SEARCH
        const val DATABASE_TABLE_SEARCH = "search"
        const val SEARCH_ID = "id"
        const val SEARCH_TEXT = "text"
        const val SEARCH_TIMESTAMP = "timestamp"
        const val SEARCH_STATUS = "status"

        const val SELECT_QUERY_SEARCH = "SELECT * FROM " + DATABASE_TABLE_SEARCH



        // SQL upit za kreiranje baze
        const val DATABASE_MUSICIAN_CREATE =
            ("CREATE TABLE " + DATABASE_TABLE_MUSICIANS
                    + " (" + MUSICIAN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                    + MUSICIAN_SPOTIFY_ID + " text NOT NULL,"
                    + MUSICIAN_NAME + " text NOT NULL,"
                    + MUSICIAN_GENRE + " text NOT NULL,"
                    + MUSICIAN_OFFICIAL_WEB + " text NOT NULL,"
                    + MUSICIAN_IMAGE_URL + " text NOT NULL);")

        // SQL upit za kreiranje baze
        const val DATABASE_SONG_CREATE =
            ("CREATE TABLE " + DATABASE_TABLE_SONGS
                    + " (" + SONG_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                    + SONG_SPOTIFY_ID + " text NOT NULL,"
                    + SONG_NAME + " text NOT NULL,"
                    + SONG_MUSICIAN_ID + " text NOT NULL,"
                    + " FOREIGN KEY (" + SONG_MUSICIAN_ID + ") REFERENCES " + DATABASE_TABLE_MUSICIANS + "(" + SONG_MUSICIAN_ID + "));")

        // SQL upit za kreiranje baze
        const val DATABASE_ALBUM_CREATE =
            ("CREATE TABLE " + DATABASE_TABLE_ALBUMS
                    + " (" + ALBUM_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                    + ALBUM_SPOTIFY_ID + " text NOT NULL,"
                    + ALBUM_NAME + " text NOT NULL,"
                    + ALBUM_SPOTIFY_LINK + " text NOT NULL,"
                    + ALBUM_MUSICIAN_ID + " text NOT NULL,"
                    + " FOREIGN KEY (" + ALBUM_MUSICIAN_ID + ") REFERENCES " + DATABASE_TABLE_MUSICIANS + "(" + ALBUM_MUSICIAN_ID + "));")

        // SQL upit za kreiranje baze
        const val DATABASE_SEARCH_CREATE =
            ("CREATE TABLE " + DATABASE_TABLE_SEARCH
                    + " (" + SEARCH_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                    + SEARCH_TEXT + " text NOT NULL,"
                    + SEARCH_TIMESTAMP + " text NOT NULL,"
                    + SEARCH_STATUS + " text NOT NULL);")


    }
}
