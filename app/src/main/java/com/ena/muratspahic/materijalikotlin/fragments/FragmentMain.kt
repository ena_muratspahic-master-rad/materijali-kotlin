package com.ena.muratspahic.materijalikotlin.fragments

import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.Button
import android.widget.EditText
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.ena.muratspahic.materijalikotlin.R
import com.ena.muratspahic.materijalikotlin.adapters.musician.MusicianAdapter
import com.ena.muratspahic.materijalikotlin.adapters.search.SearchAdapter
import com.ena.muratspahic.materijalikotlin.database.MusicianDBHelper
import com.ena.muratspahic.materijalikotlin.dialogs.CustomDialog
import com.ena.muratspahic.materijalikotlin.model.Musician
import com.ena.muratspahic.materijalikotlin.model.Search
import com.ena.muratspahic.materijalikotlin.receivers.SearchArtistResultReceiver
import com.ena.muratspahic.materijalikotlin.services.SearchArtistIntentService
import com.ena.muratspahic.materijalikotlin.utils.Constants
import java.text.SimpleDateFormat
import java.util.*

class FragmentMain : Fragment(), MusicianAdapter.OnMusicianClickListener, SearchArtistResultReceiver.Receiver, SearchAdapter.OnSearchClickListener {

    private lateinit var searchEditText: EditText
    private lateinit var searchButton: Button
    private lateinit var changeListButton: Button
    private lateinit var recyclerView: RecyclerView

    private var mData = ArrayList<Musician>()
    private lateinit var onMusicianClick: OnMusicianClick

    private var mSearches: ArrayList<Search> = arrayListOf()

    private lateinit var db: MusicianDBHelper

    private var searchesDisplayed = false
    private var searchedText : String = ""

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_main, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        if (view != null) { // Casting
            searchEditText = view!!.findViewById(R.id.searchEditText)

            searchButton = view!!.findViewById(R.id.searchButton)
            searchButton.setOnClickListener {
                searchedText = searchEditText.text.toString()
                search()
            }

            changeListButton = view!!.findViewById(R.id.changeListButton)
            changeListButton.setOnClickListener { changeView() }

            recyclerView = view!!.findViewById(R.id.listView)
            recyclerView.setHasFixedSize(true)


            db = MusicianDBHelper(context!!)
            mData = db.getAllMusicians()
            mSearches = db.getAllSearches()

            setMusicianAdapterData()
        }

        onMusicianClick = try {
            (activity as OnMusicianClick?)!!
        } catch (e: ClassCastException) {
            throw ClassCastException(activity.toString() + "Implement OnMusicianClick")
        }

        if (activity != null && activity!!.intent != null) {
            val intent = activity!!.intent
            val type = intent.type
            if (Intent.ACTION_SEND == intent.action && type != null) {
                if ("text/plain" == type) {
                    handleSendText(intent)
                }
            }
        }
    }

    private fun setMusicianAdapterData() {
        //Linear Layout manager
        val layoutManager: RecyclerView.LayoutManager = LinearLayoutManager(activity)
        recyclerView.layoutManager = layoutManager
        // ADAPTER
        val mAdapter: RecyclerView.Adapter<*> =
            context?.let { MusicianAdapter(it, mData, this) }!!
        //Povezujemo adapter na recyclerView
        recyclerView.adapter = mAdapter

    }

    private fun setSearchesAdapterData() {
        //Linear Layout manager
        val layoutManager: RecyclerView.LayoutManager = LinearLayoutManager(activity)
        recyclerView.layoutManager = layoutManager
        // ADAPTER
        val mAdapter: RecyclerView.Adapter<*> =
            context?.let { SearchAdapter(it, mSearches, this) }!!
        //Povezujemo adapter na recyclerView
        recyclerView.adapter = mAdapter
    }

    private fun search() {
        if (connectedToInternet()) {
            runArtistSearch()
        } else { // Add to listOfSearches
            addToListOfSearches(Search(searchedText, getTime(), false))
            if (searchesDisplayed) {
                setSearchesAdapterData()
            }
            showCustomDialog(
                getString(R.string.internet_title),
                getString(R.string.internet_message),
                getString(R.string.internet_button)
            )
        }
    }

    private fun showCustomDialog(titleText: String, messageText: String, buttonText: String) {
        if (context != null) {
            val dialog = CustomDialog(context!!, titleText, messageText, buttonText)

            val layoutParams = WindowManager.LayoutParams()
            layoutParams.copyFrom(dialog.window!!.attributes)
            layoutParams.width = WindowManager.LayoutParams.MATCH_PARENT
            layoutParams.height = WindowManager.LayoutParams.WRAP_CONTENT
            dialog.show()
            dialog.window!!.attributes = layoutParams
        }
    }

    private fun getTime(): String {
        val now = Date()
        val timestamp = now.time
        val simpleDateFormat = SimpleDateFormat("dd/MM/yyyy", resources.configuration.locale)
        val simpleTimeFormat = SimpleDateFormat("HH:mm:ss XXX", resources.configuration.locale)

        return simpleDateFormat.format(timestamp) + " " + getString(R.string.time) + " " + simpleTimeFormat.format(timestamp)
    }

    private fun addToListOfSearches(search: Search) {
        mSearches.add(search)
        db.addSearch(search)
    }

    private fun runArtistSearch() {
        val searchIntent = Intent(
            Intent.ACTION_SYNC, null, activity,
            SearchArtistIntentService::class.java
        )
        searchIntent.putExtra(Constants.SERVICE_SEARCH_TEXT, searchedText)
        //Result Receiver
        val mReceiver = SearchArtistResultReceiver(Handler())
        mReceiver.setReceiver(this)
        searchIntent.putExtra(Constants.SERVICE_RECEIVER, mReceiver)
        activity!!.startService(searchIntent)
    }

    private fun connectedToInternet(): Boolean {
        var connected = false
        if (activity != null && activity!!.applicationContext != null) {
            val cm =
                activity!!.applicationContext.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val nInfo = cm.activeNetworkInfo
            connected = nInfo != null && nInfo.isAvailable && nInfo.isConnected
        }
        return connected
    }

    private fun handleSendText(intent: Intent) {
        val sharedText = intent.getStringExtra(Intent.EXTRA_TEXT)
        if (sharedText != null) {
            searchEditText.setText(sharedText)
        }
    }

    interface OnMusicianClick {
        fun onMusicianClicked(musician: Musician)
    }

    override fun onMusicianClick(position: Int) {
        onMusicianClick.onMusicianClicked(mData[position])
    }

    override fun onReceiveResult(resultCode: Int, resultData: Bundle?) {
        when (resultCode) {
            0 ->  //Add something
                searchButton.setText(R.string.searching)
            1 -> {
                val musicians: ArrayList<Musician> = resultData!!.getParcelableArrayList(Constants.SERVICE_MUSICIANS)!!
                searchButton.setText(R.string.button_search)
                mData = musicians
                addToListOfSearches(Search(searchedText, getTime(), true))
                if (searchesDisplayed) {
                    changeView()
                } else {
                    setMusicianAdapterData()
                }
            }
            2 -> {
                searchButton.setText(R.string.button_search)
                searchButton.setText(R.string.button_search)
                addToListOfSearches(Search(searchedText, getTime(), false))
                val error = resultData!!.getString(Intent.EXTRA_TEXT)
                Log.e(getString(R.string.error), error)
                showCustomDialog(
                    getString(R.string.error),
                    getString(R.string.error_text),
                    getString(R.string.internet_button)
                )
            }
            else -> throw IllegalStateException("Unexpected value: $resultCode")
        }
    }

    private fun changeView() {
        if (!searchesDisplayed) {
            setSearchesAdapterData()
            changeListButton.setText(R.string.show_musician_list)
        } else {
            setMusicianAdapterData()
            changeListButton.setText(R.string.show_searches)
        }
        searchesDisplayed = !searchesDisplayed
    }

    override fun onSearchClick(position: Int) {
        // Act like click on Search button
        searchedText = mSearches[position].searchText
        search()
    }

}