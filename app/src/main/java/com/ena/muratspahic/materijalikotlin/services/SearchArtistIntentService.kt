package com.ena.muratspahic.materijalikotlin.services

import android.app.IntentService
import android.content.Intent
import android.os.Bundle
import android.os.ResultReceiver
import com.ena.muratspahic.materijalikotlin.model.Genre
import com.ena.muratspahic.materijalikotlin.model.Musician
import com.ena.muratspahic.materijalikotlin.utils.Constants
import com.ena.muratspahic.materijalikotlin.utils.Utils
import org.json.JSONException
import org.json.JSONObject
import java.io.BufferedInputStream
import java.io.IOException
import java.io.InputStream
import java.net.HttpURLConnection
import java.net.URL
import java.net.URLEncoder
import java.util.*

class SearchArtistIntentService(name: String?) : IntentService(name) {

    constructor(): this(null)

    private var musicians: ArrayList<Musician> = arrayListOf()

    override fun onHandleIntent(intent: Intent?) {
        if (intent != null) {
            val searchText = intent.getStringExtra(Constants.SERVICE_SEARCH_TEXT)
            val receiver =
                intent.getParcelableExtra<ResultReceiver>(Constants.SERVICE_RECEIVER)

            receiver.send(
                Constants.STATUS_RUNNING,
                Bundle.EMPTY
            )
            val bundle = Bundle()
            try {
                val query = URLEncoder.encode(searchText, "utf-8")
                val uri =
                    "https://api.spotify.com/v1/search?q=$query&type=artist&limit=10&offset=0"
                val url = URL(uri)
                val connection =
                    url.openConnection() as HttpURLConnection
                connection.setRequestProperty("Authorization", "Bearer " + Constants.token)
                val inputStream: InputStream =
                    BufferedInputStream(connection.inputStream)
                val result = Utils.convertStreamToString(inputStream)
                val jsonObject = JSONObject(result)
                val artists = jsonObject.getJSONObject("artists")
                val items = artists.getJSONArray("items")
                for (i in 0 until items.length()) {
                    val artist = items.getJSONObject(i)
                    // If there is no genre then don't add this artist
                    if (artist.has("genres")) {
                        val genres = artist.getJSONArray("genres")
                        if (genres != null && genres.length() > 0) {
                            val id = artist.getString("id")
                            val name = artist.getString("name")
                            val externalUrls = artist.getJSONObject("external_urls")
                            val officialWebPage = externalUrls.getString("spotify")
                            val genre = genres.getString(0)
                            val images = artist.getJSONArray("images")
                            var imageUrl = Genre.getUnknownImageResource()
                            if (images != null) {
                                val firstImage = images.getJSONObject(0)
                                imageUrl = firstImage.getString("url")
                            }
                            if (!singerExists(name)) {
                                musicians.add(
                                    Musician(
                                        id, name, genre, officialWebPage,
                                        imageUrl
                                    )
                                )
                            }
                        }
                    }
                }
                /* Proslijedi rezultate nazad u pozivatelja */
                bundle.putParcelableArrayList(
                    Constants.SERVICE_MUSICIANS,
                    musicians
                )
                receiver.send(Constants.STATUS_FINISHED, bundle)
            } catch (exc: IOException) {
                exc.printStackTrace()
                /* Vrati obavijest da je došlo do izuzetka, e – izuzetak */
                bundle.putString(
                    Intent.EXTRA_TEXT,
                    exc.toString()
                )
                receiver.send(Constants.STATUS_ERROR, bundle)
            } catch (exc: JSONException) {
                exc.printStackTrace()
                bundle.putString(Intent.EXTRA_TEXT, exc.toString())
                receiver.send(Constants.STATUS_ERROR, bundle)
            }
        }
    }

    private fun singerExists(name: String): Boolean {
        for (musician in musicians) {
            if (musician.name.equals(name)) return true
        }
        return false
    }

}