package com.ena.muratspahic.materijalikotlin.model

import com.ena.muratspahic.materijalikotlin.R

object Genre {

    const val POP = 1
    const val ELECTROPOP = 2
    const val ROCK = 3
    const val FUNK = 4
    const val INDIE_POP = 5

    fun getUnknownImageResource(): String {
        return "https://image.spreadshirtmedia.com/image-server/v1/mp/compositions/T1158A231MPA2501PT17X64Y28D1016489389S100/views/1,width=550,height=550,appearanceId=231,backgroundColor=CBCBCB,noPt=true,version=1572070041/unknown-word-black-sweatshirt-drawstring-bag.jpg"
    }

    /*
    fun getImageResource(type: String): Int {
        return when (type) {
            "Pop" -> R.drawable.pop
            "Electropop" -> R.drawable.electropop
            "Rock" -> R.drawable.rock
            "Funk" -> R.drawable.funk
            "Indie pop" -> R.drawable.indie_pop
            else -> R.drawable.unknown
        }
    } */

    fun getImageResource(type: String): Int {
        return if (type.toUpperCase().contains("POP")) {
            R.drawable.pop
        } else if (type.toUpperCase().contains("ELECTRO POP")
            || type.toUpperCase().contains("ELECTRO-POP")
            || type.toUpperCase().contains("ELECTRO POP")
        ) {
            R.drawable.electropop
        } else if (type.toUpperCase().contains("ROCK")) {
            R.drawable.rock
        } else if (type.toUpperCase().contains("FUNK")) {
            R.drawable.funk
        } else if (type.toUpperCase().contains("INDIE POP")) {
            R.drawable.indie_pop
        } else {
            R.drawable.unknown
        }
    }
}