package com.ena.muratspahic.materijalikotlin.adapters.song

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.ena.muratspahic.materijalikotlin.R
import com.ena.muratspahic.materijalikotlin.model.Song
import kotlinx.android.synthetic.main.list_element_child.view.*

class SongViewHolder(
        inflater: LayoutInflater,
        parent: ViewGroup,
        songClickCallback: (song: Song) -> Unit
) : RecyclerView.ViewHolder(inflater.inflate(R.layout.list_element_child, parent, false)) {

    lateinit var song: Song

    init {
        itemView.setOnClickListener { songClickCallback(song) }
    }


    fun bind(song: Song, position: Int) {
        this.song = song

        song.apply {
            itemView.numberTextView.text = String.format((position + 1).toString(), "%s.")
            itemView.titleTextView.text = this.name
        }
    }
}