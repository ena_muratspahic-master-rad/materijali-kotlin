package com.ena.muratspahic.materijalikotlin.adapters.album

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.ena.muratspahic.materijalikotlin.model.Album

class AlbumAdapter(private val albumClickCallback: (album : Album) -> Unit) : RecyclerView.Adapter<AlbumViewHolder>() {

    var mData = listOf<Album>()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AlbumViewHolder {
        return AlbumViewHolder(LayoutInflater.from(parent.context), parent, albumClickCallback)
    }

    override fun getItemCount(): Int {
        return mData.size
    }

    override fun onBindViewHolder(holder: AlbumViewHolder, position: Int) {
        holder.bind(mData[position], position
        )
    }

}