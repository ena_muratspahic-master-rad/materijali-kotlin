package com.ena.muratspahic.materijalikotlin.dialogs

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView
import com.ena.muratspahic.materijalikotlin.R

class CustomDialog(context: Context) : Dialog(context), View.OnClickListener {

    private var title: String? = null
    private var message: String? = null
    private var buttonText: String? = null

    constructor(context: Context, title: String?, message: String?, buttonText: String?) : this(context) {
        this.title = title
        this.message = message
        this.buttonText = buttonText
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.dialog_layout)
        setCancelable(false)
        (findViewById<View>(R.id.title) as TextView).text = title
        (findViewById<View>(R.id.message) as TextView).text = message

        val button = findViewById<Button>(R.id.button)
        button.text = buttonText
        button.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        dismiss()
    }


}