package com.ena.muratspahic.materijalikotlin.model

import android.os.Parcel
import android.os.Parcelable

class Album () : Parcelable {

    var id: Int = 0
    var spotifyId: String = ""
    var name: String = ""
    var spotifyLink: String = ""
    var musicianId: String = ""

    constructor(parcel: Parcel) : this() {
        name = parcel.readString()
        spotifyLink = parcel.readString()
    }

    constructor(id: Int, spotifyId: String, name: String, spotifyLink: String, musicianId: String) : this() {
        this.spotifyId = spotifyId
        this.name = name
        this.spotifyLink = spotifyLink
        this.musicianId = musicianId
    }

    constructor(spotifyId: String, name: String, spotifyLink: String, musicianId: String) : this() {
        this.spotifyId = spotifyId
        this.name = name
        this.spotifyLink = spotifyLink
        this.musicianId = musicianId
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(name)
        parcel.writeString(spotifyLink)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Song> {
        override fun createFromParcel(parcel: Parcel): Song {
            return Song(parcel)
        }

        override fun newArray(size: Int): Array<Song?> {
            return arrayOfNulls(size)
        }
    }


}