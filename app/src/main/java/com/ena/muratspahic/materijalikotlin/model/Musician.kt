package com.ena.muratspahic.materijalikotlin.model

import android.os.Parcel
import android.os.Parcelable

class Musician() : Parcelable {

    var id: Int = 0

    lateinit var spotifyId: String

    var name: String? = null

    var officialWebPage: String? = null

    var imageUrl: String? = null

    var genre: String? = null

    fun getGenreImageResource(): Int {
        return Genre.getImageResource(genre!!)
    }

    constructor(parcel: Parcel) : this() {
        name = parcel.readString()
        genre = parcel.readString()
        officialWebPage = parcel.readString()
    }

    constructor(id: Int, spotifyId: String, name: String, genre: String, officialWebPage: String, imageUrl: String) : this() {
        this.id = id
        this.spotifyId = spotifyId
        this.name = name
        this.genre = genre
        this.officialWebPage = officialWebPage
        this.imageUrl = imageUrl
    }

    constructor(spotifyId: String, name: String, genre: String, officialWebPage: String, imageUrl: String) : this() {
        this.spotifyId = spotifyId
        this.name = name
        this.genre = genre
        this.officialWebPage = officialWebPage
        this.imageUrl = imageUrl
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(name)
        parcel.writeString(genre)
        parcel.writeString(officialWebPage)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Musician> {
        override fun createFromParcel(parcel: Parcel): Musician {
            return Musician(parcel)
        }

        override fun newArray(size: Int): Array<Musician?> {
            return arrayOfNulls(size)
        }
    }

}