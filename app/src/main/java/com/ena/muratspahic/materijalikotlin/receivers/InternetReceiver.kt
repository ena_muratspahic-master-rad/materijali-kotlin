package com.ena.muratspahic.materijalikotlin.receivers

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.widget.Toast
import com.ena.muratspahic.materijalikotlin.R

class InternetReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context, intent: Intent) {
        if (intent.action != null && intent.action == ConnectivityManager.CONNECTIVITY_ACTION) {
            val connectivityManager =
                context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val activeNetwork = connectivityManager.activeNetworkInfo
            if (activeNetwork == null) {
                Toast.makeText(
                    context,
                    context.getString(R.string.internet_receiver_no_internet),
                    Toast.LENGTH_LONG
                ).show()
            }
        }
    }
}
