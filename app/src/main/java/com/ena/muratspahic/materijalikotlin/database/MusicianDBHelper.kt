package com.ena.muratspahic.materijalikotlin.database

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import com.ena.muratspahic.materijalikotlin.model.Album
import com.ena.muratspahic.materijalikotlin.model.Musician
import com.ena.muratspahic.materijalikotlin.model.Search
import com.ena.muratspahic.materijalikotlin.model.Song
import com.ena.muratspahic.materijalikotlin.utils.DatabaseConstants
import java.util.*

class MusicianDBHelper(context: Context) : SQLiteOpenHelper(context, DatabaseConstants.DATABASE_NAME, null, DatabaseConstants.DATABASE_VERSION) {

    /*constructor(
        context: Context?,
        name: String?,
        factory: SQLiteDatabase.CursorFactory?,
        version: Int
    ) : this(context, name, factory, version) */

    override fun onCreate(db: SQLiteDatabase?) {
        db?.let{
            db.execSQL(DatabaseConstants.DATABASE_MUSICIAN_CREATE)
            db.execSQL(DatabaseConstants.DATABASE_SONG_CREATE)
            db.execSQL(DatabaseConstants.DATABASE_ALBUM_CREATE)
            db.execSQL(DatabaseConstants.DATABASE_SEARCH_CREATE)
        }
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        // Brisanje stare verzije
        db?.let{
            db.execSQL("DROP TABLE IF EXISTS " + DatabaseConstants.DATABASE_TABLE_MUSICIANS)
            db.execSQL("DROP TABLE IF EXISTS " + DatabaseConstants.DATABASE_TABLE_SONGS)
            db.execSQL("DROP TABLE IF EXISTS " + DatabaseConstants.DATABASE_TABLE_ALBUMS)
            db.execSQL("DROP TABLE IF EXISTS " + DatabaseConstants.DATABASE_TABLE_SEARCH)

            // Kreiranje nove
            onCreate(db)
        }
    }

    // V6 - Zadatak 1
    fun deleteAll() { // Brisanje Svih podataka
        val db = this.writableDatabase
        db.delete(DatabaseConstants.DATABASE_TABLE_MUSICIANS, null, null)
        db.delete(DatabaseConstants.DATABASE_TABLE_SONGS, null, null)
        db.delete(DatabaseConstants.DATABASE_TABLE_ALBUMS, null, null)
        db.close()
    }

    // MUSICIAN
    fun addMusician(musician: Musician) {
        if (!musicianExists(musician.spotifyId)) {
            val db = this.writableDatabase
            val values = ContentValues()
            values.put(DatabaseConstants.MUSICIAN_SPOTIFY_ID, musician.spotifyId)
            values.put(DatabaseConstants.MUSICIAN_NAME, musician.name)
            values.put(DatabaseConstants.MUSICIAN_GENRE, musician.genre)
            values.put(DatabaseConstants.MUSICIAN_OFFICIAL_WEB, musician.officialWebPage)
            values.put(DatabaseConstants.MUSICIAN_IMAGE_URL, musician.imageUrl)
            db.insert(DatabaseConstants.DATABASE_TABLE_MUSICIANS, null, values)
            db.close()
        }
    }

    private fun musicianExists(spotifyId: String): Boolean {
        val db = this.readableDatabase
        val cursor = db.query(
            DatabaseConstants.DATABASE_TABLE_MUSICIANS,
            arrayOf(
                DatabaseConstants.MUSICIAN_ID, DatabaseConstants.MUSICIAN_SPOTIFY_ID
            ),
            DatabaseConstants.MUSICIAN_SPOTIFY_ID + "=?",
            arrayOf(spotifyId),
            null,
            null,
            null,
            null
        )
        if (cursor != null && cursor.moveToNext()) {
            cursor.close()
            db.close()
            return true
        }
        db.close()
        return false
    }

    fun getMusician(spotifyId: String): Musician {
        val db = this.readableDatabase
        val cursor = db.query(
            DatabaseConstants.DATABASE_TABLE_MUSICIANS,
            arrayOf(
                DatabaseConstants.MUSICIAN_ID,
                DatabaseConstants.MUSICIAN_SPOTIFY_ID,
                DatabaseConstants.MUSICIAN_NAME,
                DatabaseConstants.MUSICIAN_GENRE,
                DatabaseConstants.MUSICIAN_OFFICIAL_WEB,
                DatabaseConstants.MUSICIAN_IMAGE_URL
            ),
            DatabaseConstants.MUSICIAN_SPOTIFY_ID + "=?",
            arrayOf(spotifyId),
            null,
            null,
            null,
            null
        )
        var musician: Musician = Musician()
        if (cursor != null && cursor.moveToNext()) {
            musician = Musician(
                cursor.getString(0).toInt(),
                cursor.getString(1),
                cursor.getString(2),
                cursor.getString(3),
                cursor.getString(4),
                cursor.getString(5)
            )
            cursor.close()
        }
        db.close()
        return musician
    }

    fun getMusicianName(musicianId: String): String {
        var artistName = ""
        val db = this.readableDatabase
        val cursor = db.query(
            DatabaseConstants.DATABASE_TABLE_MUSICIANS,
            arrayOf(
                DatabaseConstants.MUSICIAN_SPOTIFY_ID, DatabaseConstants.MUSICIAN_NAME
            ),
            DatabaseConstants.MUSICIAN_SPOTIFY_ID + "=?",
            arrayOf(musicianId),
            null,
            null,
            null,
            null
        )
        if (cursor != null && cursor.moveToNext()) {
            artistName = cursor.getString(1)
            cursor.close()
        }
        db.close()
        return artistName
    }

    fun getAllMusicians(): ArrayList<Musician> {
        val musicians: ArrayList<Musician> = ArrayList<Musician>()
        val SELECT_QUERY =
            "SELECT * FROM " + DatabaseConstants.DATABASE_TABLE_MUSICIANS
        val db = this.readableDatabase
        val cursor = db.rawQuery(SELECT_QUERY, null)
        if (cursor.moveToFirst()) {
            do {
                val musician = Musician()
                musician.id = cursor.getInt(0)
                musician.spotifyId = cursor.getString(1)
                musician.name = cursor.getString(2)
                musician.genre = cursor.getString(3)
                musician.officialWebPage = cursor.getString(4)
                musician.imageUrl = cursor.getString(5)
                musicians.add(musician)
            } while (cursor.moveToNext())
        }
        db.close()
        cursor.close()
        return musicians
    }

    private fun deleteMusician(musician: Musician) {
        val db = this.writableDatabase
        db.delete(
            DatabaseConstants.DATABASE_TABLE_MUSICIANS,
            DatabaseConstants.MUSICIAN_ID + "=?",
            arrayOf(java.lang.String.valueOf(musician.id))
        )
        db.close()
    }

    private fun getMusicianCount(): Int {
        val countQuery = "SELECT * FROM " + DatabaseConstants.DATABASE_TABLE_MUSICIANS
        val db = this.readableDatabase
        val cursor = db.rawQuery(countQuery, null)
        val count = cursor.count
        cursor.close()
        return count
    }

    // SONG
    private fun getContentValueForSong(song: Song): ContentValues {
        val values = ContentValues()
        values.put(DatabaseConstants.SONG_SPOTIFY_ID, song.spotifyId)
        values.put(DatabaseConstants.SONG_NAME, song.name)
        values.put(DatabaseConstants.SONG_MUSICIAN_ID, song.musicianId)
        return values
    }

    fun addSongs(songs: ArrayList<Song>) {
        val db = this.writableDatabase
        for (i in songs.indices) {
            val song = getContentValueForSong(songs[i])
            db.insert(DatabaseConstants.DATABASE_TABLE_SONGS, null, song)
        }
        db.close()
    }

    private fun deleteSong(song: Song) {
        val db = this.writableDatabase
        db.delete(
            DatabaseConstants.DATABASE_TABLE_SONGS,
            DatabaseConstants.SONG_ID + "=?",
            arrayOf(java.lang.String.valueOf(song.id))
        )
        db.close()
    }

    fun getAllSongsFromMusician(musicianId: String): ArrayList<Song> {
        val songs: ArrayList<Song> = ArrayList<Song>()
        val db = this.readableDatabase
        val cursor = db.query(
            DatabaseConstants.DATABASE_TABLE_SONGS,
            arrayOf(
                DatabaseConstants.SONG_ID,
                DatabaseConstants.SONG_SPOTIFY_ID,
                DatabaseConstants.SONG_NAME,
                DatabaseConstants.SONG_MUSICIAN_ID
            ),
            DatabaseConstants.SONG_MUSICIAN_ID + "=?",
            arrayOf(musicianId),
            null,
            null,
            null,
            null
        )
        if (cursor != null && cursor.moveToFirst()) {
            do {
                val song = Song(
                    cursor.getInt(0),
                    cursor.getString(1),
                    cursor.getString(2),
                    cursor.getString(3)
                )
                songs.add(song)
            } while (cursor.moveToNext())
            cursor.close()
        }
        db.close()
        return songs
    }

    fun getSongsCount(): Int {
        val countQuery = "SELECT * FROM " + DatabaseConstants.DATABASE_TABLE_SONGS
        val db = this.readableDatabase
        val cursor = db.rawQuery(countQuery, null)
        val count = cursor.count
        cursor.close()
        return count
    }


    // ALBUM
    private fun getContentValuesForAlbum(album: Album): ContentValues {
        val values = ContentValues()
        values.put(DatabaseConstants.ALBUM_SPOTIFY_ID, album.spotifyId)
        values.put(DatabaseConstants.ALBUM_NAME, album.name)
        values.put(DatabaseConstants.ALBUM_SPOTIFY_LINK, album.spotifyLink)
        values.put(DatabaseConstants.ALBUM_MUSICIAN_ID, album.musicianId)
        return values
    }

    fun addAlbums(albums: ArrayList<Album>) {
        val db = this.writableDatabase
        for (i in albums.indices) {
            val album = getContentValuesForAlbum(albums[i])
            db.insert(DatabaseConstants.DATABASE_TABLE_ALBUMS, null, album)
        }
        db.close()
    }

    private fun deleteAlbum(album: Album) {
        val db = this.writableDatabase
        db.delete(
            DatabaseConstants.DATABASE_TABLE_ALBUMS,
            DatabaseConstants.ALBUM_ID + "=?",
            arrayOf(java.lang.String.valueOf(album.id))
        )
        db.close()
    }


    fun getAllAlbumsFromMusician(musicianId: String): ArrayList<Album> {
        val albums: ArrayList<Album> = ArrayList<Album>()
        val db = this.readableDatabase
        val cursor = db.query(
            DatabaseConstants.DATABASE_TABLE_ALBUMS,
            arrayOf(
                DatabaseConstants.ALBUM_ID,
                DatabaseConstants.ALBUM_SPOTIFY_ID,
                DatabaseConstants.ALBUM_NAME,
                DatabaseConstants.ALBUM_SPOTIFY_LINK,
                DatabaseConstants.ALBUM_MUSICIAN_ID
            ),
            DatabaseConstants.ALBUM_MUSICIAN_ID + "=?",
            arrayOf(musicianId),
            null,
            null,
            null,
            null
        )
        if (cursor != null && cursor.moveToFirst()) {
            do {
                val album = Album(
                    cursor.getInt(0),
                    cursor.getString(1),
                    cursor.getString(2),
                    cursor.getString(3),
                    cursor.getString(4)
                )
                albums.add(album)
            } while (cursor.moveToNext())
            cursor.close()
        }
        db.close()
        return albums
    }

    fun getAlbumsCount(): Int {
        val countQuery = "SELECT * FROM " + DatabaseConstants.DATABASE_TABLE_ALBUMS
        val db = this.readableDatabase
        val cursor = db.rawQuery(countQuery, null)
        val count = cursor.count
        cursor.close()
        return count
    }

    fun getFirstMusicianSpotifyId(): String? {
        var musicianSpotifyId: String? = null
        val db = this.readableDatabase
        val cursor = db.query(
            DatabaseConstants.DATABASE_TABLE_MUSICIANS, arrayOf(
                DatabaseConstants.MUSICIAN_ID, DatabaseConstants.MUSICIAN_SPOTIFY_ID
            ),
            DatabaseConstants.MUSICIAN_ID + "=?", arrayOf("1"), null, null, null, null
        )
        if (cursor != null && cursor.moveToNext()) {
            musicianSpotifyId = cursor.getString(1)
            cursor.close()
        }
        db.close()
        return musicianSpotifyId
    }

    // SEARCH
    fun addSearch(search: Search) {
        val db = this.writableDatabase
        val values = ContentValues()
        values.put(DatabaseConstants.SEARCH_TEXT, search.searchText)
        values.put(DatabaseConstants.SEARCH_TIMESTAMP, search.timeStamp)
        values.put(DatabaseConstants.SEARCH_STATUS, search.status.toString())
        db.insert(DatabaseConstants.DATABASE_TABLE_SEARCH, null, values)
        db.close()
    }

    fun getAllSearches(): ArrayList<Search> {
        val searches: ArrayList<Search> = arrayListOf()

        val db = this.readableDatabase
        val cursor = db.rawQuery(DatabaseConstants.SELECT_QUERY_SEARCH, null)
        if (cursor.moveToFirst()) {
            do {
                val search = Search()
                search.id = cursor.getInt(0)
                search.searchText = cursor.getString(1)
                search.timeStamp = cursor.getString(2)
                search.status = cursor.getString(3) == "true"
                searches.add(search)
            } while (cursor.moveToNext())
        }
        cursor.close()
        db.close()
        return searches
    }

    // DELETE FUNCTIONS

    // DELETE FUNCTIONS
    fun deleteAllButLastFive() {
        val musicians: ArrayList<Musician> = getAllMusicians()
        for (i in 0 until getMusicianCount() - 5) {
            val musician: Musician = musicians[i]
            deleteMusician(musician)
            deleteMusicianSongs(musician.spotifyId)
            deleteMusicianAlbums(musician.spotifyId)
        }
    }

    private fun deleteMusicianSongs(spotifyId: String) {
        val songs: ArrayList<Song> = getAllSongsFromMusician(spotifyId)
        for (i in songs.indices) {
            val song: Song = songs[i]
            deleteSong(song)
        }
    }

    private fun deleteMusicianAlbums(spotifyId: String) {
        val albums: ArrayList<Album> = getAllAlbumsFromMusician(spotifyId)
        for (i in albums.indices) {
            val album: Album = albums[i]
            deleteAlbum(album)
        }
    }


}