package com.ena.muratspahic.materijalikotlin.receivers

import android.os.Bundle
import android.os.Handler
import android.os.ResultReceiver

class SearchArtistResultReceiver(handler: Handler?) : ResultReceiver(handler) {

    private lateinit var mReceiver: Receiver

    fun setReceiver(receiver: Receiver) {
        mReceiver = receiver
    }

    interface Receiver {
        fun onReceiveResult(resultCode: Int, resultData: Bundle?)
    }

    override fun onReceiveResult(resultCode: Int, resultData: Bundle?) {
        mReceiver.onReceiveResult(resultCode, resultData)
    }
    
}