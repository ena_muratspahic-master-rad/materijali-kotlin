package com.ena.muratspahic.materijalikotlin.fragments

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.AdapterView.OnItemClickListener
import android.widget.ListView
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import com.ena.muratspahic.materijalikotlin.R
import com.ena.muratspahic.materijalikotlin.adapters.album.AlbumAdapter
import com.ena.muratspahic.materijalikotlin.adapters.song.SongAdapter
import com.ena.muratspahic.materijalikotlin.database.MusicianDBHelper
import com.ena.muratspahic.materijalikotlin.model.Song
import com.ena.muratspahic.materijalikotlin.utils.Constants
import kotlinx.android.synthetic.main.child_fragment.*
import java.util.*

class ChildFragmentSongs : Fragment() {

    private lateinit var mSongs: ArrayList<Song>
    private lateinit var musicianName: String

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.child_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        childTextView.text = getString(R.string.top_songs)

        if (arguments != null && arguments!!.containsKey(Constants.MUSICIAN_SPOTIFY_ID)) {
            val musicianId = arguments!!.getString(Constants.MUSICIAN_SPOTIFY_ID)

            if (musicianId != null && context != null) {
                // Get from database and set album adapter
                val db = MusicianDBHelper(context!!)
                mSongs = db.getAllSongsFromMusician(musicianId)
                musicianName = db.getMusicianName(musicianId)
                setSongsAdapter()
            }
        }
    }

    private fun setSongsAdapter() {
        val songsAdapter = SongAdapter(::songsClicked)
        childListView.apply {
            layoutManager = LinearLayoutManager(context)
            itemAnimator = DefaultItemAnimator()
            adapter = songsAdapter
        }
        songsAdapter.mData = mSongs

    }

    private fun songsClicked(song: Song) {
        val intent = Intent(Intent.ACTION_SEARCH)
        intent.setPackage("com.google.android.youtube")
        intent.putExtra(
            "query",
            musicianName + " - " + song.name
        )
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(intent)
    }

}