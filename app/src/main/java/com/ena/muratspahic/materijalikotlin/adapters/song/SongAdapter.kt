package com.ena.muratspahic.materijalikotlin.adapters.song

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.ena.muratspahic.materijalikotlin.model.Song

class SongAdapter(private val songClickCallback: (song : Song) -> Unit) : RecyclerView.Adapter<SongViewHolder>() {

    var mData = listOf<Song>()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SongViewHolder {
        return SongViewHolder(LayoutInflater.from(parent.context), parent, songClickCallback)
    }

    override fun getItemCount(): Int {
        return mData.size
    }

    override fun onBindViewHolder(holder: SongViewHolder, position: Int) {
        holder.bind(mData[position], position)
    }
}