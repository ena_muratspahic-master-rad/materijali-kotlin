package com.ena.muratspahic.materijalikotlin.adapters.search

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.ena.muratspahic.materijalikotlin.R
import com.ena.muratspahic.materijalikotlin.model.Search

class SearchAdapter (private var context: Context,
                     private var mData: ArrayList<Search>,
                     onSearchClickListener: OnSearchClickListener) : RecyclerView.Adapter<RecyclerView.ViewHolder>(){

    var mOnSearchListener: OnSearchClickListener? = null

    init {
        mOnSearchListener = onSearchClickListener

    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val inflater = LayoutInflater.from(context)
        return SearchHolder(
                inflater.inflate(R.layout.list_search_element, parent, false), mOnSearchListener!!)
    }

    override fun getItemCount(): Int {
        return mData.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val search = mData[position]

        val searchHolder = holder as SearchHolder
        searchHolder.searchTextView.text = search.searchText
        searchHolder.timestampTextView.text = search.timeStamp

        val statusColor = if (search.status) R.color.search_success else R.color.search_fail
        holder.searchContainer.setBackgroundColor(holder.itemView.context.resources.getColor(statusColor))

    }

    internal class SearchHolder(itemView: View, onSearchClickListener: OnSearchClickListener) : RecyclerView.ViewHolder(itemView), View.OnClickListener {
        var searchTextView: TextView = itemView.findViewById(R.id.searchedTextView) as TextView
        var timestampTextView: TextView = itemView.findViewById(R.id.timeTextView) as TextView

        var searchContainer: ConstraintLayout = itemView.findViewById(R.id.container_search) as ConstraintLayout

        private var onSearchListener: OnSearchClickListener? = null

        init {
            onSearchListener = onSearchClickListener
            itemView.setOnClickListener(this)
        }
        override fun onClick(v: View?) {
            onSearchListener?.onSearchClick(adapterPosition)
        }
    }

    interface OnSearchClickListener {
        fun onSearchClick(position: Int)
    }

}