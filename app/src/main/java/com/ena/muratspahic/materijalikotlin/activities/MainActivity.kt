package com.ena.muratspahic.materijalikotlin.activities

import android.content.IntentFilter
import android.net.ConnectivityManager
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.ena.muratspahic.materijalikotlin.R
import com.ena.muratspahic.materijalikotlin.database.MusicianDBHelper
import com.ena.muratspahic.materijalikotlin.fragments.FragmentMain
import com.ena.muratspahic.materijalikotlin.fragments.FragmentMusician
import com.ena.muratspahic.materijalikotlin.model.Musician
import com.ena.muratspahic.materijalikotlin.receivers.InternetReceiver
import com.ena.muratspahic.materijalikotlin.utils.Constants
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), FragmentMain.OnMusicianClick {

    private var wide = false

    // Database
    private lateinit var db: MusicianDBHelper

    private val internetReceiver: InternetReceiver = InternetReceiver()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // Database init
        db = MusicianDBHelper(this)

        //Dohvatanje FragmentManager-a
        val fragmentManager = supportFragmentManager
        if (fragmentTwo != null) {
            wide = true
            var fragmentMusician = fragmentManager.findFragmentById(R.id.fragmentTwo)
            if (fragmentMusician == null) {
                fragmentMusician = FragmentMusician()
                val spotifyIdFirst = db.getFirstMusicianSpotifyId()
                spotifyIdFirst?.let {
                    val arguments = Bundle()
                    arguments.putString(Constants.MUSICIAN_SPOTIFY_ID, spotifyIdFirst)
                    fragmentMusician.arguments = arguments
                }
                fragmentManager.beginTransaction().replace(R.id.fragmentTwo, fragmentMusician)
                    .commit()
            }
        }

        var fragmentMain= fragmentManager.findFragmentByTag("Main")
        if (fragmentMain == null) {
            fragmentMain = FragmentMain()
            fragmentManager.beginTransaction().replace(R.id.fragmentOne, fragmentMain, "Main")
                .commit()
        }
    }

    override fun onResume() {
        super.onResume()
        registerReceiver(internetReceiver, IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION))
    }

    override fun onPause() {
        super.onPause()
        unregisterReceiver(internetReceiver)
    }

    override fun onMusicianClicked(musician: Musician) {
        // Add to database
        db.addMusician(musician)

        val arguments = Bundle()
        arguments.putString(Constants.MUSICIAN_SPOTIFY_ID, musician.spotifyId)
        val fragmentMusician = FragmentMusician()
        fragmentMusician.arguments = arguments
        if (wide) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.fragmentTwo, fragmentMusician)
                .addToBackStack(null)
                .commit()
        } else {
            supportFragmentManager.beginTransaction()
                .replace(R.id.fragmentOne, fragmentMusician)
                .addToBackStack(null)
                .commit()
        }
    }

    override fun onStop() {
        super.onStop()
        // V6 - Zadatak 1
        //db.deleteAll();
        db.deleteAllButLastFive()
    }

}
