package com.ena.muratspahic.materijalikotlin.services

import android.app.IntentService
import android.content.Intent
import android.os.Bundle
import android.os.ResultReceiver
import com.ena.muratspahic.materijalikotlin.model.Song
import com.ena.muratspahic.materijalikotlin.utils.Constants
import com.ena.muratspahic.materijalikotlin.utils.Utils
import org.json.JSONException
import org.json.JSONObject
import java.io.BufferedInputStream
import java.io.IOException
import java.io.InputStream
import java.net.HttpURLConnection
import java.net.URL
import java.net.URLEncoder
import java.util.*

class SongsIntentService(name: String?) : IntentService(name) {

    constructor(): this(null)

    private var songs: ArrayList<Song> = arrayListOf()

    override fun onHandleIntent(intent: Intent?) {
        if (intent != null) {
            val musicianId = intent.getStringExtra(Constants.SERVICE_MUSICIAN_ID)
            val receiver =
                intent.getParcelableExtra<ResultReceiver>(Constants.SERVICE_RECEIVER)
            /* Update UI: Početak taska */
            receiver.send(
                Constants.STATUS_RUNNING,
                Bundle.EMPTY
            )
            val bundle = Bundle()
            try {
                val query = URLEncoder.encode(musicianId, "utf-8")
                val uri =
                    "https://api.spotify.com/v1/artists/$query/top-tracks?country=US"
                val url = URL(uri)
                val connection = url.openConnection() as HttpURLConnection
                connection.setRequestProperty("Authorization", "Bearer " + Constants.token)
                val inputStream: InputStream = BufferedInputStream(connection.inputStream)
                val result: String = Utils.convertStreamToString(inputStream)
                val jsonObject = JSONObject(result)
                val items = jsonObject.getJSONArray("tracks")
                for (i in 0 until items.length()) {
                    val song = items.getJSONObject(i)
                    val id = song.getString("id")
                    val songName = song.getString("name")
                    songs.add(Song(id, songName, musicianId))
                }
                /* Proslijedi rezultate nazad u pozivatelja */
                bundle.putParcelableArrayList(
                    Constants.SERVICE_SONGS,
                    songs
                )
                receiver.send(Constants.STATUS_FINISHED, bundle)
            } catch (exc: IOException) {
                exc.printStackTrace()
                /* Vrati obavijest da je došlo do izuzetka, e – izuzetak */
                bundle.putString(
                    Intent.EXTRA_TEXT,
                    exc.toString()
                )
                receiver.send(Constants.STATUS_ERROR, bundle)
            } catch (exc: JSONException) {
                exc.printStackTrace()
                bundle.putString(Intent.EXTRA_TEXT, exc.toString())
                receiver.send(Constants.STATUS_ERROR, bundle)
            }
        }
    }

}