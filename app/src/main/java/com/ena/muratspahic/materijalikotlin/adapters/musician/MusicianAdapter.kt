package com.ena.muratspahic.materijalikotlin.adapters.musician

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.ena.muratspahic.materijalikotlin.R
import com.ena.muratspahic.materijalikotlin.model.Musician

class MusicianAdapter (private var context: Context,
                       private var mData: ArrayList<Musician>,
                       onMusicianClickListener: OnMusicianClickListener) : RecyclerView.Adapter<RecyclerView.ViewHolder>(){

    var mOnMusicianListener: OnMusicianClickListener? = null

    init {
        mOnMusicianListener = onMusicianClickListener

    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val inflater = LayoutInflater.from(context)
        return MusicianHolder(
                inflater.inflate(R.layout.list_element, parent, false), mOnMusicianListener!!)
    }

    override fun getItemCount(): Int {
        return mData.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val musician = mData[position]

        val musicianHolder = holder as MusicianHolder
        musicianHolder.genreImageView.setImageResource(musician.getGenreImageResource())
        musicianHolder.nameTextView.text = musician.name
        musicianHolder.genreTextView.text = musician.genre
    }

    internal class MusicianHolder(itemView: View, onMusicianClickListener: OnMusicianClickListener) : RecyclerView.ViewHolder(itemView), View.OnClickListener {
        var genreImageView: ImageView = itemView.findViewById(R.id.image) as ImageView
        var nameTextView: TextView = itemView.findViewById(R.id.nameTextView) as TextView
        var genreTextView: TextView = itemView.findViewById(R.id.genreTextView) as TextView

        private var onMusicianListener: OnMusicianClickListener? = null

        init {
            onMusicianListener = onMusicianClickListener
            itemView.setOnClickListener(this)
        }
        override fun onClick(v: View?) {
            onMusicianListener?.onMusicianClick(adapterPosition)
        }
    }

    interface OnMusicianClickListener {
        fun onMusicianClick(position: Int)
    }

}