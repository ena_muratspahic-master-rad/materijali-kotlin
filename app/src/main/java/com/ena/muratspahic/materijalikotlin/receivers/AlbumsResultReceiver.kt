package com.ena.muratspahic.materijalikotlin.receivers

import android.os.Bundle
import android.os.Handler
import android.os.ResultReceiver

class AlbumsResultReceiver(handler: Handler?) : ResultReceiver(handler) {

    private lateinit var mReceiver: Receiver

    fun setReceiver(receiver: Receiver) {
        mReceiver = receiver
    }

    interface Receiver {
        fun onAlbumsReceiveResult(resultCode: Int, resultData: Bundle?)
    }

    override fun onReceiveResult(resultCode: Int, resultData: Bundle?) {
        mReceiver.onAlbumsReceiveResult(resultCode, resultData)
    }
}
