package com.ena.muratspahic.materijalikotlin.utils

class Constants {

    companion object {

        // SERVICE
        const val SERVICE_SEARCH_TEXT = "SERVICE_SEARCH_TEXT"
        const val SERVICE_RECEIVER = "SERVICE_RECEIVER"
        const val SERVICE_MUSICIANS = "SERVICE_MUSICIANS"
        const val SERVICE_SONGS = "SERVICE_SONGS"
        const val SERVICE_ALBUMS = "SERVICE_ALBUMS"

        const val SERVICE_MUSICIAN_ID = "SERVICE_MUSICIAN_ID"

        const val STATUS_RUNNING = 0
        const val STATUS_FINISHED = 1
        const val STATUS_ERROR = 2

        const val MUSICIAN_SPOTIFY_ID = "MUSICIAN_SPOTIFY_ID"

        const val recipientForSendEmailIntent = "vidimmonitor@gmail.com"

        const val token : String = "BQBLZa6fMKKl1Jarfv4IUqxk9HeXr4la25bkjbssVlkGdZmaZgAZMmK48Z1AsnJa9FuYUHPM4ikdZ7nCB4VlyRf87ROa26gMEYjpUHi7RNMn0jnUejeKopOieHAUzCB1xThfsw2RSh3PuWBUi1cjz-PHQbkCwb8jxWcm1sSNd0ioFw4WRcE35GAz2p_va6K6Ek8ja5soi6ivaaZ7pRIy__3IeozUfMF3_CaF0wwJbRI2RWIq7kV80xC2Hgb_Wiszw7u78LDrGL1336O__WHt8gTJ5mxnmg"

    }
}