package com.ena.muratspahic.materijalikotlin.fragments

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.*
import androidx.fragment.app.Fragment
import com.ena.muratspahic.materijalikotlin.R
import com.ena.muratspahic.materijalikotlin.database.MusicianDBHelper
import com.ena.muratspahic.materijalikotlin.dialogs.CustomDialog
import com.ena.muratspahic.materijalikotlin.model.Album
import com.ena.muratspahic.materijalikotlin.model.Musician
import com.ena.muratspahic.materijalikotlin.model.Song
import com.ena.muratspahic.materijalikotlin.receivers.AlbumsResultReceiver
import com.ena.muratspahic.materijalikotlin.receivers.SongsResultReceiver
import com.ena.muratspahic.materijalikotlin.services.AlbumsIntentService
import com.ena.muratspahic.materijalikotlin.services.SongsIntentService
import com.ena.muratspahic.materijalikotlin.utils.Constants
import com.squareup.picasso.Picasso
import java.util.*

class FragmentMusician: Fragment(), SongsResultReceiver.Receiver, AlbumsResultReceiver.Receiver {

    private lateinit var mMusician: Musician

    private var songViewIsDisplayed = false

    private lateinit var frameLayoutChild: FrameLayout

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view: View = inflater.inflate(R.layout.fragment_musician, container, false)
        if (arguments != null && arguments!!.containsKey(Constants.MUSICIAN_SPOTIFY_ID)) {

            val spotifyId = arguments!!.getString(Constants.MUSICIAN_SPOTIFY_ID)!!

            val db = context?.let {
                MusicianDBHelper(it)
            }

            val musicianImageView = view.findViewById<ImageView>(R.id.genreImageView)
            val shareButton = view.findViewById<Button>(R.id.shareButton)
            val changeViewButton = view.findViewById<Button>(R.id.changeViewButton)

            if(db != null) {
                mMusician = db.getMusician(spotifyId)

                //Loading image using Picasso
                Picasso.get().load(mMusician.imageUrl).into(musicianImageView)

                val nameTextView = view.findViewById<TextView>(R.id.nameTextView)
                nameTextView.text = mMusician.name

                val officialLinkTextView = view.findViewById<TextView>(R.id.officialLinkTextView)
                officialLinkTextView.text = mMusician.officialWebPage
                officialLinkTextView.setOnClickListener { openOfficialPage() }

                val genreTextView = view.findViewById<TextView>(R.id.genreTextView)
                genreTextView.text = mMusician.genre

                shareButton.setOnClickListener { sendBioToOtherApp() }

                changeViewButton.setOnClickListener {
                    // Change text on the button and change view
                    changeViewButton.text =
                        if (songViewIsDisplayed) getString(R.string.top_songs) else getString(
                            R.string.similar_musicians
                        )
                    changeView()
                }

                frameLayoutChild = view.findViewById(R.id.fragmentChild)

                searchMusicianTopSongs(mMusician.spotifyId)
                searchMusicianAlbums(mMusician.spotifyId)

               // changeView()

            } else {
                musicianImageView.visibility = View.GONE
                shareButton.visibility = View.GONE
                changeViewButton.visibility = View.GONE
            }
        }
        return view
    }

    private fun searchMusicianTopSongs(spotifyId: String) {
        val searchIntent = Intent(
            Intent.ACTION_SYNC, null, activity,
            SongsIntentService::class.java
        )
        searchIntent.putExtra(Constants.SERVICE_MUSICIAN_ID, spotifyId)
        //Result Receiver
        val mReceiver = SongsResultReceiver(Handler())
        mReceiver.setReceiver(this)
        searchIntent.putExtra(Constants.SERVICE_RECEIVER, mReceiver)
        activity!!.startService(searchIntent)
    }

    private fun searchMusicianAlbums(spotifyId: String) {
        val searchIntent = Intent(
            Intent.ACTION_SYNC, null, activity,
            AlbumsIntentService::class.java
        )
        searchIntent.putExtra(Constants.SERVICE_MUSICIAN_ID, spotifyId)
        //Result Receiver
        val mReceiver = AlbumsResultReceiver(Handler())
        mReceiver.setReceiver(this)
        searchIntent.putExtra(Constants.SERVICE_RECEIVER, mReceiver)
        activity!!.startService(searchIntent)
    }

    private fun changeView() {
        val arguments = Bundle()
        arguments.putString(Constants.MUSICIAN_SPOTIFY_ID, mMusician.spotifyId)
        if (!songViewIsDisplayed) {
            setSongsView(arguments)
        } else {
            setAlbumsView(arguments)
        }
        songViewIsDisplayed = !songViewIsDisplayed
    }

    private fun setSongsView(arguments: Bundle) {
        val fragmentManager = childFragmentManager
        val childFragmentTopFive = ChildFragmentSongs()
        val transaction = fragmentManager.beginTransaction()

        childFragmentTopFive.arguments = arguments
        transaction.replace(R.id.fragmentChild, childFragmentTopFive)
        transaction.commit()
    }

    private fun setAlbumsView(arguments: Bundle) {
        val fragmentManager = childFragmentManager
        val childFragmentAlbums = ChildFragmentAlbums()
        val transaction = fragmentManager.beginTransaction()

        childFragmentAlbums.arguments = arguments
        transaction.replace(R.id.fragmentChild, childFragmentAlbums)
        transaction.commit()
    }


    private fun openOfficialPage() {
        val openOfficialPage = Intent(Intent.ACTION_VIEW)
        openOfficialPage.data = Uri.parse(mMusician.officialWebPage)
        startActivity(openOfficialPage)
    }

    private fun sendBioToOtherApp() {
        val sendEmailIntent = Intent(Intent.ACTION_SEND)
        sendEmailIntent.type = "text/plain"
        sendEmailIntent.putExtra(
            Intent.EXTRA_EMAIL,
            arrayOf(Constants.recipientForSendEmailIntent)
        )
        sendEmailIntent.putExtra(Intent.EXTRA_SUBJECT, mMusician.name)
        sendEmailIntent.putExtra(Intent.EXTRA_TEXT, mMusician.officialWebPage)
        if (activity != null) {
            if (sendEmailIntent.resolveActivity(activity!!.packageManager) != null) {
                startActivity(sendEmailIntent)
            }
        }
    }

    override fun onSongsReceiveResult(resultCode: Int, resultData: Bundle?) {
        when (resultCode) {
            0 -> return
            1 -> {
                val songs: ArrayList<Song> = resultData!!.getParcelableArrayList(Constants.SERVICE_SONGS)!!
                // Add to database
                val db = MusicianDBHelper(activity!!)
                db.addSongs(songs)
                changeView()
            }
            2 -> {
                val error = resultData!!.getString(Intent.EXTRA_TEXT)
                Log.e(getString(R.string.error), error)
                showCustomDialog(getString(R.string.error), getString(R.string.error_text), getString(R.string.internet_button))
            }
            else -> throw IllegalStateException("Unexpected value: $resultCode")
        }
    }

    override fun onAlbumsReceiveResult(resultCode: Int, resultData: Bundle?) {
        when (resultCode) {
            0 -> return
            1 -> {
                val albums: ArrayList<Album> = resultData!!.getParcelableArrayList(Constants.SERVICE_ALBUMS)!!
                // Add to database
                val db = MusicianDBHelper(activity!!)
                db.addAlbums(albums)
            }
            2 -> {
                val error = resultData!!.getString(Intent.EXTRA_TEXT)
                Log.e(getString(R.string.error), error)
                showCustomDialog(getString(R.string.error), getString(R.string.error_text), getString(R.string.internet_button))
            }
            else -> throw IllegalStateException("Unexpected value: $resultCode")
        }
    }

    private fun showCustomDialog(titleText: String, messageText: String, buttonText: String) {
        if (context != null) {
            val dialog = CustomDialog(context!!, titleText, messageText, buttonText)

            val layoutParams = WindowManager.LayoutParams()
            layoutParams.copyFrom(dialog.window!!.attributes)
            layoutParams.width = WindowManager.LayoutParams.MATCH_PARENT
            layoutParams.height = WindowManager.LayoutParams.WRAP_CONTENT
            dialog.show()
            dialog.window!!.attributes = layoutParams
        }
    }
}